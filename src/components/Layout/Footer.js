import React from 'react';

import { Layout } from 'antd';

const Footer = () => {
  return (
    <Layout.Footer style={{ textAlign: 'center' }}></Layout.Footer>
  );
};

export default Footer;
