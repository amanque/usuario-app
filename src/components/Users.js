import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Table } from "antd";

import columns from "../features/users/components/columns";
import {
  actionCreators as usersActions,
  selector as usersSelector,
} from "../features/users";

const Users = () => {
  const { users } = useSelector((state) => usersSelector(state));
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(usersActions.list());
  }, [dispatch]);

  return (
    <div>
      <h2>Listado de Usuarios</h2>
      <Table
        scroll={{ x: 400 }}
        columns={columns}
        dataSource={users.dataList}
        loading={users.isLoading}
        rowKey={"email"}
      />
    </div>
  );
};

export default Users;
