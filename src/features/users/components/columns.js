import React from "react";
import { Tag } from "antd";

const columns = [
  {
    title: "Email",
    dataIndex: "email",
    key:"email",
    responsive: ["xs"]
  },
  {
    title: "Password",
    dataIndex: "password",
    key:"password",
    responsive: ["sm"]
  },
  {
    title: "Phones",
    dataIndex: "phones",
    key:"phones",
    responsive: ["sm"],
    render: (phones) => (
      <>
        {phones.map((phone) => {
          return (
            <div>
              <Tag key={phone.number+"1"}>Number: {phone.number}</Tag>
              <Tag key={phone.number+"2"}>City Code: {phone.cityCode}</Tag>
              <Tag key={phone.number+"3"}>Contry Code: {phone.contryCode}</Tag>
            </div>
          );
        })}
      </>
    ),
  },
  {
    title: "ID",
    dataIndex: "idUser",
    key:"idUser",
    responsive: ["sm"]
  },
];

export default columns;
