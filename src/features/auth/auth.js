import { createStructuredSelector } from "reselect"

export const NAME = "auth"

// Action Types
const DATA_TRY = "[auth]/DATA_TRY"
const DATA_SUCCESS = "[auth]/DATA_SUCCESS"
const DATA_FAILED = "[auth]/DATA_FAILED"

// Initial State:
const initialState = {
  data: {},
  isLoading: false,
};

// REDUCER:
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case actionTypes.DATA_TRY:
      return { ...state, isLoading: true }
    case actionTypes.DATA_FAILED:
      return { ...state, isLoading: false }
    case actionTypes.DATA_SUCCESS:
      return { ...state, isLoading: false, data: action.data }
    default:
      return state
  }
}

function getAuth() {
    return { type: DATA_TRY }
  }
  
  const auth = (state) => state[NAME]
  
  export const selector = createStructuredSelector({
    auth,
  })
  
  export const actionCreators = {
    getAuth
  }

export const actionTypes = {
  DATA_TRY,
  DATA_FAILED,
  DATA_SUCCESS,
}