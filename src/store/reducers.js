import {combineReducers} from 'redux';
import auth from '../features/auth';

import usersReducer, {NAME as usersName} from '../features/users'
import authReducer, {NAME as authName} from '../features/auth'

export default () => 
    combineReducers({
    [usersName]: usersReducer,
    [authName]: authReducer
});