import {takeLatest} from 'redux-saga/effects'
import {actionTypes as usersActions} from '../features/users';
import {actionTypes as authActions} from '../features/auth';

import tryUsersList from './users/list'
import tryAuthData from './auth/getAuth'

export default function* rootSaga() {
    yield takeLatest(usersActions.LIST_TRY, tryUsersList);
    yield takeLatest(authActions.DATA_TRY, tryAuthData)
}