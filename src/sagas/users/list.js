import { call, put } from "redux-saga/effects";
import ServiceFactory from "../../config/ServiceFactory";

import { actionTypes as userActions } from "../../features/users";

let service = new ServiceFactory();
export default function* tryUsersList() {
  try {
    const token = (state) => state.auth;
    let response = yield call(service.get, { url: "/user/lista", token });
    const data = response.data.object;
    yield put({ type: userActions.LIST_SUCCESS, data });
  } catch (e) {
    yield put({ type: userActions.LIST_FAILED });
  }
}
