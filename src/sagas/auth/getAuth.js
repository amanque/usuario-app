import { call, put } from 'redux-saga/effects'
import ServiceFactory from '../../config/ServiceFactory';
import { params } from "./../../constants/Constants";

import { actionTypes as authActions } from '../../features/auth';

let service = new ServiceFactory()
export default function* tryAuthData(){
    try {
        const response = yield call(service.post, {url: '/authentication', params});
        const data =  response.data.object;
        yield put({ type: authActions.DATA_SUCCESS, data })
    } catch (e) {
        yield put({ type: authActions.DATA_FAILED })
    }
}