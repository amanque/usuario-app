import axios from "axios";
import { baseURL } from "./../constants/Proxy";

export const axiosInstance = axios.create({
  baseURL: baseURL.REACT_APP.URL.API_URL,
});

class ServiceFactory {

  async post(url, params) {
    return axiosInstance.post(url, params);
  }

  async get(param, token) {
    let response = null;
    axios.defaults.headers.common.Authorization = `Bearer ${token}`;
    if (axios.defaults.headers.common.Authorization !== undefined) {
      response = await axiosInstance.get(param.url);
    }
    return response;
  }
}

export default ServiceFactory;
